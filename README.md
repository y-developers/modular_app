[![Release](https://jitpack.io/v/com.gitlab.y-developers/modular_app.svg)]
(https://jitpack.io/#com.gitlab.y-developers/modular_app)
# Java "Modular App"

## Using this project

You can use this project via gradle, maven, sbt and leiningen, by requiring it as dependency from jitpack.
[More info](https://jitpack.io/#com.gitlab.y-developers/modular_app)

## About this project
This project provides some classes, to help you, to create a modular application, where you can load, unload and reload 
single parts without restarting the whole app.

## How to use
Using this is as easy, as extending a single class. Just extend in you main class (e.g. App) the `ModularApp` class.
With this class, you have to implement the methods `getModules()` and `getMechanics()`. While the `getModules()` method
returns an array of `Module` instances, the `getMechanics()` methods returns an array of `Mechanic` instances.
When someone is now creating a new instance of the `App` class, its iterating through those modules, and pass each of them,
to every registered mechanic. The mechanic then, can call methods, provided by interfaces, and implemented in the module.
