import de.yannik_sc.modular_app.modules.Module;

/**
 * @author Yannik_Sc
 */
public class ExampleModule extends Module
{
    @Override
    public String getName()
    {
        return "Example Module";
    }

    @Override
    public void boot() throws Exception
    {
        System.out.println("I'm getting booted :3");
    }
}
