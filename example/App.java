import de.yannik_sc.modular_app.ModularApp;
import de.yannik_sc.modular_app.mechanics.Mechanic;
import de.yannik_sc.modular_app.modules.Module;

/**
 * @author Yannik_Sc
 */
public class App extends ModularApp
{
    @Override
    protected Module[] getModules()
    {
        return new Module[]{
            new ExampleModule()
        };
    }

    @Override
    protected Mechanic[] getMechanics()
    {
        return new Mechanic[]{
            new ExampleMechanic()
        };
    }

    public static void main(String[] args)
    {
        new App();
    }
}
