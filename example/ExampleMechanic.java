import de.yannik_sc.modular_app.mechanics.Mechanic;
import de.yannik_sc.modular_app.modules.Module;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yannik_Sc
 */
public class ExampleMechanic extends Mechanic
{
    protected List<String> loadedModules = new ArrayList<>();

    @Override
    public String getName()
    {
        return "Example Mechanic";
    }

    /**
     * Adds the module name to the loadedModules list
     *
     * @param module the instance of the module
     */
    @Override
    public void onBoot(Module module)
    {
        this.loadedModules.add(module.getName());
    }

    /**
     * Removes the modules name from the loadedModules list
     *
     * @param module the instance of the module
     */
    @Override
    public void onShutdown(Module module)
    {
        this.loadedModules.remove(module.getName());
    }

    /**
     * Does nothing, the modules name has not to be restored into the list
     *
     * @param module the instance of the module
     */
    @Override
    public void onReload(Module module)
    {
    }
}
