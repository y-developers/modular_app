package de.yannik_sc.modular_app.mechanics;

import de.yannik_sc.modular_app.modules.Module;

/**
 * @author Yannik_Sc
 */
public abstract class Mechanic
{
    /**
     * Gets the name of the mechanic
     *
     * @return the name of the mechanic
     */
    public abstract String getName();

    /**
     * Doing something with the module, or not, if the module does not match the requirements (e.g. does not implement
     * interface XYZ)
     * Called after the modules `boot` method was called
     *
     * @param module the instance of the module
     */
    public abstract void onBoot(Module module);

    /**
     * Doing something with the module, or not, if the module does not match the requirements (e.g. does not implement
     * interface XYZ)
     * Called after the modules `shutdown` method was called
     *
     * @param module the instance of the module
     */
    public abstract void onShutdown(Module module);

    /**
     * Doing something with the module, or not, if the module does not match the requirements (e.g. does not implement
     * interface XYZ)
     * Called after the modules `reload` method was called
     *
     * @param module the instance of the module
     */
    public abstract void onReload(Module module);
}
