package de.yannik_sc.modular_app.modules;

/**
 * @author Yannik_Sc
 */
public enum Ability
{
    BOOT, SHUTDOWN, RELOAD
}
