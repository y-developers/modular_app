package de.yannik_sc.modular_app.modules;

/**
 * @author Yannik_Sc
 */
public abstract class Module
{
    /**
     * Gets the name of the module
     *
     * @return
     */
    public abstract String getName();

    public abstract void boot() throws Exception;

    public Ability[] getAbilities()
    {
        return new Ability[]{
            Ability.BOOT
        };
    }

    public void shutdown() throws Exception
    {
    }

    public void reload() throws Exception
    {
    }
}
