package de.yannik_sc.modular_app;

import de.yannik_sc.modular_app.mechanics.Mechanic;
import de.yannik_sc.modular_app.modules.Ability;
import de.yannik_sc.modular_app.modules.Module;

import java.util.Arrays;
import java.util.List;

/**
 * @author Yannik_Sc
 */
public abstract class ModularApp
{
    protected List<Mechanic> mechanics;

    /**
     * @return all initially loaded modules
     */
    protected abstract Module[] getModules();

    /**
     * @return all initially loaded mechanics
     */
    protected abstract Mechanic[] getMechanics();

    public ModularApp()
    {
        this.mechanics = Arrays.asList(this.getMechanics());

        this.prepare();
        this.init();
    }

    /**
     * Boots the module; calls its `boot` method, and invokes the `onBoot` method of all registered mechanics with the
     * module as parameter
     *
     * @param module the module to boot
     */
    public void bootModule(Module module)
    {
        if (!Arrays.asList(module.getAbilities()).contains(Ability.BOOT)) {
            System.err.println(String.format("Module %s does not support action BOOT", module.getName()));

            return;
        }

        System.out.println(String.format("Booting module %s", module.getName()));

        try {
            module.boot();
        } catch (Exception e) {
            System.err.println(String.format("Could not boot module %s", module.getName()));
            e.printStackTrace();

            return;
        }

        for (Mechanic mechanic : this.mechanics) {
            mechanic.onBoot(module);
        }

        System.out.println(String.format("Module %s booted", module.getName()));
    }

    /**
     * Shutdowns the module; calls its `shutdown` method, and invokes the `onShutdown` method of all registered mechanics
     * with the module as parameter
     *
     * @param module the module to boot
     */
    public void shutdownModule(Module module)
    {
        if (!Arrays.asList(module.getAbilities()).contains(Ability.SHUTDOWN)) {
            System.err.println(String.format("Module %s does not support action SHUTDOWN", module.getName()));

            return;
        }

        System.out.println(String.format("Shutting down module %s", module.getName()));

        try {
            module.shutdown();
        } catch (Exception e) {
            System.err.println(String.format("Could not shutdown module %s", module.getName()));
            e.printStackTrace();

            return;
        }

        for (Mechanic mechanic : this.mechanics) {
            mechanic.onShutdown(module);
        }

        System.out.println(String.format("Shutted down module %s", module.getName()));
    }

    /**
     * Boots the module; calls its `boot` method, and invokes the `onBoot` method of all registered mechanics with the
     * module as parameter
     *
     * @param module the module to boot
     */
    public void reloadModule(Module module)
    {
        if (!Arrays.asList(module.getAbilities()).contains(Ability.RELOAD)) {
            System.err.println(String.format("Module %s does not support action RELOAD", module.getName()));

            return;
        }

        System.out.println(String.format("Reloading module %s", module.getName()));

        try {
            module.reload();
        } catch (Exception e) {
            System.err.println(String.format("Could not reload module %s", module.getName()));
            e.printStackTrace();

            return;
        }

        for (Mechanic mechanic : this.mechanics) {
            mechanic.onReload(module);
        }

        System.out.println(String.format("Module %s reloaded", module.getName()));
    }

    /**
     * Initially setups variables, or creates services, like an event system for the modules to communicate
     */
    protected void prepare()
    {
    }

    protected void init()
    {
        for (Module module : this.getModules()) {
            this.bootModule(module);
        }
    }
}
